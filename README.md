# tweetrepeater
This repeats tweets, every x miliseconds.

## Requirements

* NodeJS, last LTS version
* "twitter" package
* "fs" package
* Knowledge

## Install

* git clone
* nano bot.js
* *fill the app info*
* fill the groups and actions txts with whatever you want. Actions being the first part, groups being the second one.
* node kpopper.js
 

## Change delay

```javascript
setTimeout(tweet, 3600000)};
```
orders the bot to tweet, every 3600000 ms, by default. Change this default value by whatever you want. **Twitter doesn't like spam bots, so be careful.**


## Credits :

Farid Ikiker for helping me fix the shiity code provided by @irchuxy.
